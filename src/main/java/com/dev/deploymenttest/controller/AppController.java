package com.dev.deploymenttest.controller;


import com.dev.deploymenttest.dto.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AppController {


    @GetMapping("/")
    public List<Response> getResponse() {
        List<Response> responseList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            responseList.add(Response.builder()
                    .message("ti pidor")
                    .title("ya net")
                    .build());
        }
        return responseList;
    }
}
